import React from "react"
import LandingPage from '../components/landing-page';
import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <div className='center'>
    <Layout title='Buy Now'>
      <SEO title="Home" />
      <LandingPage />
    </Layout>
  </div>
)

export default IndexPage
