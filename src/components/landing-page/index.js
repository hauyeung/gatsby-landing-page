import React from "react"
import BannerImage from './banner.jpg'

const LandingPage = () => {
  return (
    <div className='center'>
      <h1 >Buy Now</h1>
      <p>Use Bit to store your code now.</p>
      <div style={{ maxWidth: `100vw`, marginBottom: `1.45rem` }}>
        <img src={BannerImage} alt='landing page image' />
      </div>
      <button>Subscribe to Bit Now</button>
    </div>
  )
}

export default LandingPage
